#property copyright ""
#property link      ""
#property description ""
#property version "2.02"
#property strict
#property show_inputs

int ExtHandle = -1;
static int ArrPeriod[];

void OnStart(){
  int period_ = Period();
  if(period_ != PERIOD_M1){
    Alert("Start Period Must be M1!!!");
    return;
  }

  ArrayResize(ArrPeriod, 8);
  ArrPeriod[0] = 5;
  ArrPeriod[1] = 15;
  ArrPeriod[2] = 30;
  ArrPeriod[3] = 60;
  ArrPeriod[4] = 240;
  ArrPeriod[5] = 1440;
  ArrPeriod[6] = 10080;
  ArrPeriod[7] = 43200;
  int periodseconds;
  datetime time0;
  double d_open=0, d_low=0, d_high=0, d_close=0, d_volume=0, last_volume;
  int hwnd = 0, i_time=0;

  //---- History header
  int    version = 400;
  string c_copyright = "(C)opyright 2003, MetaQuotes Software Corp.";
  string c_symbol = Symbol();
  int    i_period = 1;
  int    i_digits = Digits;
  int    i_unused[13];
  string Comm = ""; 
  int start_pos = Bars - 1;

  int ArrPeriod_size = ArraySize(ArrPeriod);
  for(int array_num=0; array_num<ArrPeriod_size; array_num++){
    i_period = period_*ArrPeriod[array_num];
    periodseconds = i_period*60;
    ExtHandle = FileOpenHistory(c_symbol + (string)i_period + ".hst", FILE_BIN|FILE_WRITE);
    if(ExtHandle < 0){
      Print("file open faild");
      return;
    }

    //---- write history file header
    FileWriteInteger(ExtHandle, version, LONG_VALUE);
    FileWriteString(ExtHandle, c_copyright, 64);
    FileWriteString(ExtHandle, c_symbol, 12);
    FileWriteInteger(ExtHandle, i_period, LONG_VALUE);
    FileWriteInteger(ExtHandle, i_digits, LONG_VALUE);
    FileWriteInteger(ExtHandle, 0, LONG_VALUE);       //timesign
    FileWriteInteger(ExtHandle, 0, LONG_VALUE);       //last_sync
    FileWriteArray(ExtHandle, i_unused, 0, 13);

    //---- write history file
    for(int i=start_pos-1; i>=0; i--){
      if(i >= start_pos-1){
        // 時間足単位の秒数で割り算して切り下げ整数値を算出
        // 例えばTime[start_pos]が1709618490、periodsecondsが2592000(43200*60)の場合
        // 1709618490/2592000 = 659.575.....
        // itime = 659
        i_time = int(Time[i+1] / periodseconds);
        // 上記で算出したi_timeにperiodsecondsが2592000(43200*60)を掛ける
        // 659 * 2592000 = 1708128000
        i_time *= periodseconds;
        d_open = Open[i+1];
        d_low = Low[i+1];
        d_high = High[i+1];
        d_volume = (double)Volume[i+1];
      }

      time0 = Time[i];
      int timedayofweek = TimeDayOfWeek(time0);
      if(timedayofweek==0 || timedayofweek==6){
        Comment("saturday! or Sunday candle skip. time : ", TimeToStr(time0, TIME_DATE|TIME_SECONDS));
        // 土曜日、日曜日の場合はループをcontinueする
        continue;
      }

      int time_month = TimeMonth(time0);
      int time_day = TimeDay(time0);
      if((time_month==12 && time_day==25) || (time_month==1 && time_day==1) || (time_month==1 && time_day==2 && timedayofweek==1)){
        Comment("12/25 or 1/1 or (1/1&mondaiy) candle skip.  time : ", TimeToStr(time0, TIME_DATE|TIME_SECONDS));
        // 12月25日と1月1日, 1月2日が月曜日の場合はcontinue
        continue;
      }

      if((i_time + periodseconds)<=time0 || i==0){
        if(i==0 && (time0<(i_time + periodseconds))){
          d_volume += (double)Volume[0];
          if(Low[0] < d_low){ d_low = Low[0]; }
          if(High[0] > d_high){ d_high = High[0]; }
          d_close = Close[0];
        }

        last_volume = (double)Volume[i];
        FileWriteInteger(ExtHandle, i_time, LONG_VALUE);
        FileWriteDouble(ExtHandle, d_open, DOUBLE_VALUE);
        FileWriteDouble(ExtHandle, d_low, DOUBLE_VALUE);
        FileWriteDouble(ExtHandle, d_high, DOUBLE_VALUE);
        FileWriteDouble(ExtHandle, d_close, DOUBLE_VALUE);
        FileWriteDouble(ExtHandle, d_volume, DOUBLE_VALUE);
        FileFlush(ExtHandle);

        if(time0 >= i_time + periodseconds){
          i_time = int(time0 / periodseconds);
          i_time *= periodseconds;
          d_open = Open[i];
          d_low = Low[i];
          d_high = High[i];
          d_close = Close[i];
          d_volume = last_volume;
        }
      }else{
        d_volume += (double)Volume[i];
        if(Low[i] < d_low){ d_low = Low[i]; }
        if(High[i] > d_high){ d_high = High[i]; }
        d_close=Close[i];
      }
    }
    FileFlush(ExtHandle);
    FileCloseFunction(ExtHandle);
    Comm = "Converting to Period (" + (string)i_period + "), bars to end: ";
    Comment(Comm);
  }
}

void OnDeinit(const int reason){
  FileCloseFunction(ExtHandle);
  Comment("");
  Print("period converter done.");
}

void FileCloseFunction(int &filehandle){
  if(filehandle>= 0){
    FileClose(filehandle);
    filehandle = -1;
  }
}
